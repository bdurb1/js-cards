const axios = require('axios')


const getDeck = () => {
  axios.get(`https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1`)
  .then(res => {
  deckId = res.data.deck_id
  setTimeout(getTwo, 1000)
  })
}


 getTwo = async () => {
   let queens = 0
   let arr = []
   while(queens <= 4) {
    const res = await axios.get(`https://deckofcardsapi.com/api/deck/${deckId}/draw/?count=2`)
      arr.push(res.data.cards[0])
      arr.push(res.data.cards[1])
      if(res.data.cards[0].value == "QUEEN" || res.data.cards[1].value == "QUEEN") {
        queens++
      } else if(res.data.cards[0].value == "QUEEN" && res.data.cards[1].value == "QUEEN") {
        queens + 2
      }
      
    }
    sortArrs(arr)
  }
  
  const sortArrs = (arr) => {
    arr.forEach(card => {
      if(card.value == "ACE") {
        card.code = 1
      } else if (card.value == "JACK") {
        card.code = 11
      } else if (card.value == "QUEEN") {
        card.code = 12
      } else if (card.value == "KING") {
        card.code = 13
      } else {
        card.code = Number(card.value)
      }
    })
    const spades = []
    const spadesSort = []
    const hearts = []
    const heartsSort = []
    const diamonds = []
    const diamondsSort = []
    const clubs = []
    const clubsSort = []
    arr.forEach(card => {
      if(card.suit == "SPADES") {
        spades.push(card)
      } else if (card.suit == "HEARTS") {
        hearts.push(card)
      } else if (card.suit == "DIAMONDS") {
        diamonds.push(card)
      } else if (card.suit == "CLUBS") {
        clubs.push(card)
      }
    })
    spades.sort((a, b) => a.code - b.code).forEach(spade => spadesSort.push(spade.value))
    hearts.sort((a, b) => a.code - b.code).forEach(heart => heartsSort.push(heart.value))
    diamonds.sort((a, b) => a.code - b.code).forEach(diamond => diamondsSort.push(diamond.value))
    clubs.sort((a, b) => a.code - b.code).forEach(club => clubsSort.push(club.value))

    console.log('spades:', spadesSort)
    console.log('hearts:', heartsSort)
    console.log('diamonds:', diamondsSort)
    console.log('clubs:', clubsSort)
  }


getDeck()
    
